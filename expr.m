:- module expr.

:- interface.
:- import_module nat, show, list, bool, maybe.
% TODO: move showable instances to read.m or its own module

:- type expr
  ---> indexed(indexed_expr)
     ; named(named_expr)
     .
:- instance showable(expr).

:- pragma terminates(show_coloured/1).
:- func show_coloured(expr) = string.

:- type index ---> idx(nat) ; wildcard.

% TODO: unify indexed and named expressions to reduce duplication?
% Bindings could simply have maybe a name and maybe an index

% de Bruijn indexed variables
:- type indexed_expr
  ---> var(index)
     ; app(indexed_expr, indexed_expr)
     % original name (if any), domain type and body
     ; lambda({maybe(string), indexed_expr}, indexed_expr)
     % original name (if any), domain type and body
     ; pi_type({maybe(string), indexed_expr}, indexed_expr)
     % constants/builtins
     ; sort(sort)
     ; bool_type
     ; literal(lit)
     .
:- instance showable(indexed_expr).

% named variables
:- type named_expr
  ---> var(var)
     ; app(named_expr, named_expr)
     % lambda abstraction: λx:A.b
     ; lambda(binding, named_expr)
     % dependent function type: Πx:A.B(x)
     ; pi_type(binding, named_expr)
     % constants/builtins
     ; sort(sort)
     ; bool_type
     ; literal(lit)
     .
:- instance showable(named_expr).

:- pred is_atom(named_expr::in) is semidet.

:- type lit
 ---> boolean(bool)
    .
:- instance showable(lit).

:- type var ---> v(string) ; wildcard.
:- instance showable(var).

:- type sort ---> typ(nat).

:- type binding ---> l(var, named_expr).
:- instance showable(binding).

:- type type_env(E) == list({maybe(string), E}).
% pretty-printing the context for readability.
:- pragma terminates(show_context/1).
:- func show_context(type_env(indexed_expr)) = string.


:- func unname(indexed_expr) = indexed_expr.

:- type indexing_result
 ---> ok(indexed_expr)
    ; free_variable(type_env(named_expr), string, named_expr)
    % If a wildcard is found in the body of an expression.
    % TODO: maybe named_expr.var should be a string and we add to bindings a sum type with wildcards,
    % then remove this.
    ; hole(type_env(named_expr), named_expr)
    .

:- func to_de_bruijn(type_env(named_expr), named_expr) = indexing_result is det.
:- instance showable(indexing_result).

:- func from_de_bruijn(type_env(indexed_expr), indexed_expr) = named_expr is det.

:- pragma terminates(normalise/1).
:- func normalise(indexed_expr) = indexed_expr. % strongly

% substitute(M, I, N) = E -- E is the result of substituting variables of index I for N in M.
% NOTE:
% 1. This is supposed to be used on the body containing the free variable,
%    not the expression containing it.
%    (That is, you substitute on the body of an abstraction or dependent type.)
% 2. This is only used when eliminating an abstraction: thus any index higher than I gets
%    decremented (as they need to step over one less abstraction)
:- pragma terminates(substitute/3).
:- func substitute(indexed_expr, nat, indexed_expr) = indexed_expr.

% update(E, K, I) = E_ -- update indices in E referencing free variables
% (E = var(N), N >= K) to account for their new abstraction depth (I)
% after having been substituted into it.
% In other words: K is the number of variables locally bound in the abstraction
% being moved and I is the number of additional variables bound after it's moved
% which it now has to hop over in order to reference the old variables.
:- pragma terminates(update/3).
:- func update(indexed_expr, nat, nat) = indexed_expr.

:- implementation.
:- import_module utils, pprint, string, int.
:- use_module enum, require.

is_atom(literal(_)).
is_atom(var(_)).
is_atom(app(_, _)).
is_atom(sort(_)).
is_atom(bool_type).

:- instance showable(indexing_result) where [
    show(ok(E)) = show(E),
    show(free_variable(Ctx, Str, E)) = "Free variable " ++ Str ++ " in expression:\n" ++ show(E) ++ "\nin context:\n" ++ show(Ctx),
    show(hole(Ctx, E)) = "Found hole in expression:\n" ++ show(E) ++ "\nin context:\n" ++ show(Ctx)
].

:- instance showable(named_expr) where [
    show(literal(Lit)) = show(Lit),
    show(var(V)) = show(V),
    show(app(M, N)) =
        ( if is_atom(M)
        then show(M) ++ "(" ++ show(N) ++ ")"
        else "(" ++ show(M) ++ ")(" ++ show(N) ++ ")"
        ),
    show(lambda(Ann, E)) = string.format("%s ↦ %s", [s(show(Ann)), s(show(E))]),
    show(pi_type(Ann@l(V, A), E)) = Str :-
        ( if named(var(V)) `occurs_in` named(E)
        then Str = string.format("%s → %s", [s(show(Ann)), s(show(E))])
        else Str =  string.format("%s → %s", [s(show(A)), s(show(E))])
        ),
    show(sort(typ(zero))) = "★",
    show(sort(typ(succ(zero)))) = "◻",
    show(sort(typ(L@succ(succ(_))))) = string.format("#T(%s)", [s(show(L))]),
    show(bool_type) = "#B"
].

:- instance showable(lit) where [
    show(boolean(yes)) = "#t",
    show(boolean(no)) = "#f"
].
:- instance showable(var) where [
    show(v(S)) = S,
    show(wildcard) = "_"
    % show(V) = Str :- promise_equivalent_solutions [Str] (V = v(Str) ; V = wildcard, Str = "_")
].
:- instance showable(binding) where [
    show(l(V, A)) = string.format("(%s : %s)", [s(show(V)), s(show(A))])
].
:- instance showable(index) where [
    show(idx(N)) = "#" ++ show(N),
    show(wildcard) = "_"
].

:- instance showable(expr) where [
  show(indexed(E)) = show(E),
  show(named(E)) = show(E)
].

show_context([]) = "()".
show_context(Ctx@[_|_]) = Ctx_ :-
    CtxRev = list.reverse(Ctx),
    list.foldl2(
        (pred(T::in, Acc1::in, Str::out, Acc2::in, [T|Acc2]::out) is det :-
            T = {MV, A},
            AExpr = from_de_bruijn(Acc2, A) : named_expr,
            ( if MV = yes(V)
            then Str = show(V) ++ " : " ++ show(AExpr) ++ "\n" ++ Acc1
            else Str = show(AExpr) ++ "\n" ++ Acc1
            )
        ),
        CtxRev,
        "",
        Ctx_,
        [],
        _
    ).

show_coloured(named(E)) = show_named_coloured([], E).
show_coloured(indexed(E)) = show_indexed_coloured([], E).

:- func show_named_coloured(list(named_expr), named_expr) = string.
show_named_coloured(_, var(V)) =
    clear ++ Str ++ clear :-
        (
            V = v(S),
            Str = colour_code(green, high_intensity) ++ S
        ;
            V = wildcard,
            Str = colour_code(white, boldhigh_intensity) ++ "_"
        ).
show_named_coloured(Ctx, lambda(l(V, A), E)) =
    clear ++
    colour_code(blue, high_intensity) ++
    "(" ++
    colour_code(green, regular) ++
    show(V) ++
    colour_code(blue, high_intensity) ++
    " : " ++
    show_named_coloured(Ctx, A) ++
    colour_code(blue, high_intensity) ++
    ") ↦ " ++
    show_named_coloured([A|Ctx], E) ++
    clear.
show_named_coloured(Ctx, pi_type(l(V, A), E)) =
    clear ++
    ( if named(var(V)) `occurs_in` named(E)
    then
        colour_code(purple, high_intensity) ++
        "(" ++
        colour_code(green, regular) ++
        show(V) ++
        colour_code(purple, high_intensity) ++
        " : " ++
        show_named_coloured(Ctx, A) ++
        colour_code(purple, high_intensity) ++
        ") → " ++
        show_named_coloured([A|Ctx], E) ++
        colour_code(purple, high_intensity)
    else
        colour_code(purple, high_intensity) ++
        show_named_coloured(Ctx, A) ++
        colour_code(purple, high_intensity) ++
        " → " ++
        show_named_coloured([A|Ctx], E) ++
        colour_code(purple, high_intensity)
    ) ++
    clear.
show_named_coloured(_, literal(X)) =
    clear ++
    colour_code(red, regular) ++
    show(X) ++
    clear.
show_named_coloured(_, sort(typ(L))) =
    clear ++
    colour_code(white, boldhigh_intensity) ++
    "#T" ++
    clear ++
    colour_code(yellow, high_intensity) ++
    "(" ++
    colour_code(red, regular) ++
    show(L) ++
    colour_code(yellow, high_intensity) ++
    ")" ++
    clear.
show_named_coloured(_, B@bool_type) =
    clear ++
    colour_code(white, boldhigh_intensity) ++
    show(B) ++
    clear.
show_named_coloured(Ctx, app(M, N)) =
    clear ++
    show_named_coloured(Ctx, M) ++
    colour_code(yellow, high_intensity) ++
    "(" ++
    show_named_coloured(Ctx, N) ++
    colour_code(yellow, high_intensity) ++
    ")" ++
    clear.

:- instance showable(indexed_expr) where [
    show(var(V)) = show(V),
    show(lambda({yes(V), A}, E)) = string.format("<%s:%s>%s", [s(V), s(show(A)), s(show(E))]),
    show(lambda({no, A}, E)) = string.format("<%s>%s", [s(show(A)), s(show(E))]),
    show(pi_type({yes(V), A}, E)) = string.format("[%s:%s]%s", [s(V), s(show(A)), s(show(E))]),
    show(pi_type({no, A}, E)) = string.format("[%s]%s", [s(show(A)), s(show(E))]),
    show(literal(Lit)) = show(Lit),
    show(sort(typ(L))) = string.format("(%s)#T", [s(show(L))]),
    show(bool_type) = "#B",
    show(app(M, N)) = string.format("(%s)%s", [s(show(N)), s(show(M))])
].

:- func idx_colour(list(indexed_expr), nat) = colour.
idx_colour([], _) = black.
idx_colour([_|Ctx], zero) = colour(from_int_det(list.length(Ctx))).
idx_colour([_|Ctx], succ(N)) = idx_colour(Ctx, N).

% get colour of variable introduced earlier
:- func show_indexed_coloured(list(indexed_expr), indexed_expr) = string.
show_indexed_coloured(Ctx, var(V)) =
    clear ++
    ( if V = idx(N)
    then
        colour_code(idx_colour(Ctx, N), high_intensity) ++
        show(V)
    else
        colour_code(white, boldhigh_intensity) ++
        show(V)
    ) ++
    clear.

show_indexed_coloured(Ctx, lambda({_, A}, E)) =
    clear ++
    colour_code(Clr, boldhigh_intensity) ++
    "<" ++
    show_indexed_coloured(Ctx, A) ++
    colour_code(Clr, boldhigh_intensity) ++
    ">" ++
    clear ++
    show_indexed_coloured([A|Ctx], E) :-
        Clr = colour(nat.from_int_det(list.length(Ctx))).
show_indexed_coloured(Ctx, pi_type({_, A}, E)) =
    clear ++
    colour_code(Clr, boldhigh_intensity) ++
    "[" ++
    show_indexed_coloured(Ctx, A) ++
    colour_code(Clr, boldhigh_intensity) ++
    "]" ++
    clear ++
    show_indexed_coloured([A|Ctx], E) :-
        Clr = colour(nat.from_int_det(list.length(Ctx))).
show_indexed_coloured(_, literal(X)) =
    clear ++
    colour_code(white, boldhigh_intensity) ++
    show(X) ++
    clear.
show_indexed_coloured(_, sort(typ(L))) =
    clear ++
    "(" ++
    colour_code(white, boldhigh_intensity) ++
    show(L) ++
    clear ++
    ")" ++
    colour_code(white, boldhigh_intensity) ++
    "#T" ++
    clear.
show_indexed_coloured(_, B@bool_type) =
    clear ++
    colour_code(white, boldhigh_intensity) ++
    show(B) ++
    clear.
show_indexed_coloured(Ctx, app(M, N)) =
    clear ++
    "(" ++
    show_indexed_coloured(Ctx, N) ++
    clear ++
    ")" ++
    show_indexed_coloured(Ctx, M) ++
    clear.


normalise(V@var(_)) = V.
normalise(X@literal(_)) = X.
normalise(bool_type) = bool_type.
normalise(S@sort(_)) = S.
normalise(lambda({Aliases, A}, E)) = E_ :-
    E_ = lambda({Aliases, normalise(A)}, normalise(E)).
normalise(pi_type({Aliases, A}, B)) = E_ :-
    E_ = pi_type({Aliases, normalise(A)}, normalise(B)).
normalise(app(M, N)) = E_ :-
    ( M_ = normalise(M) & N_ = normalise(N) ),
    ( if M_ = lambda(_, Body)
    then
        E_ = normalise(substitute(Body, zero, N_))
    else
        E_ = app(M_, N_)
    ).

substitute(V@var(wildcard), _, _) = V.
substitute(var(idx(zero)), zero, E) = update(E, zero, zero).
substitute(var(idx(zero)), succ(_), _) = var(idx(zero)).
substitute(var(idx(N@succ(J))), I, E) = M :-
        nat_compare(C, N, I),
        (
            C = (>),
            % this variable refers to a binding outside of the abstraction we're eliminating,
            % so we decrement it since it has one less abstraction to hop over
            M = var(idx(J))
        ;
            C = (<),
            % this variable is local to an abstraction contained inside
            % the abstraction we're eliminating, so it remains unaffected
            M = var(idx(N))
        ;
            C = (=),
            % this is the variable to substitute
            M = update(E, zero, I)
        ).
substitute(app(M1, N1), I, E) = app(substitute(M1, I, E), substitute(N1, I, E)).
substitute(lambda({Aliases, A}, Body), I, E) = lambda({Aliases, substitute(A, I, E)}, substitute(Body, succ(I), E)).
substitute(pi_type({Aliases, A}, Body), I, E) = pi_type({Aliases, substitute(A, I, E)}, substitute(Body, succ(I), E)).
substitute(S@sort(_), _, _) = S.
substitute(X@literal(_), _, _) = X.
substitute(X@bool_type, _, _) = X.

update(V@var(wildcard), _, _) = V.
update(var(idx(N)), K, I) =
    ( if nat_compare((<), N, K)
    then % this variable is locally bound and should not be updated
        var(idx(N))
    else % this index refers to a free variable, so we update it to account for its new abstraction depth
        var(idx(add(N, I)))
    ).
update(app(M, N), K, I) = app(update(M, K, I), update(N, K, I)).
update(lambda({Aliases, A}, Body), K, I) = lambda({Aliases, update(A, K, I)}, update(Body, succ(K), I)).
update(pi_type({Aliases, A}, Body), K, I) = pi_type({Aliases, update(A, K, I)}, update(Body, succ(K), I)).
update(S@sort(_), _, _) = S.
update(X@literal(_), _, _) = X.
update(X@bool_type, _, _) = X.

%%% Conversion to and from de Bruijn indexed expressions

to_de_bruijn(Ctx, E) = ResIE :-
    (
        E = var(v(V)),
        ( if % it's not a name but an index
            string.append("#", IStr, V),
            string.to_int(IStr, Int),
            I = enum.from_int(Int)
        then ResIE = ok(var(idx(I)))
        else if I = get_index(Ctx, V, zero)
        then ResIE = ok(var(idx(I)))
        else ResIE = free_variable(Ctx, V, E)
        )

    ;
        E = var(wildcard),
        ResIE = ok(var(wildcard : index))

    ;
        E = lambda(l(V, A), Body),
        ( V = v(V_), MV = yes(V_)
        ; V = wildcard, MV = no
        ),
        ResIA = to_de_bruijn(Ctx, A),
        (
            ResIA = ok(A_),
            InnerCtx = [{MV, A}|Ctx],
            ResIB = to_de_bruijn(InnerCtx, Body),
            (
                ResIB = ok(IB),
                ResIE = ok(lambda({MV, A_}, IB))
            ;
                ResIB = free_variable(_, V__, _),
                ResIE = free_variable(InnerCtx, V__, E)
            ;
                ResIB = hole(_, _),
                ResIE = hole(InnerCtx, E)
            )
        ;
            ResIA = free_variable(_, V__, _),
            ResIE = free_variable(Ctx, V__, E)
        ;
            ResIA = hole(_, _),
            ResIE = hole(Ctx, E)
        )

    ;
        E = pi_type(l(V, A), Body),
        ( V = v(V_), MV = yes(V_)
        ; V = wildcard, MV = no
        ),
        ResIA = to_de_bruijn(Ctx, A),
        (
            ResIA = ok(A_),
            InnerCtx = [{MV, A}|Ctx],
            ResIB = to_de_bruijn(InnerCtx, Body),
            (
                ResIB = ok(IB),
                ResIE = ok(pi_type({MV, A_}, IB))
            ;
                ResIB = free_variable(_, V__, _),
                ResIE = free_variable(InnerCtx, V__, E)
            ;
                ResIB = hole(_, _),
                ResIE = hole(InnerCtx, E)
            )
        ;
            ResIA = free_variable(_, V__, _),
            ResIE = free_variable(Ctx, V__, E)
        ;
            ResIA = hole(_, _),
            ResIE = hole(Ctx, E)
        )

    ;
        E = sort(S),
        ResIE = ok(sort(S))

    ;
        E = literal(Lit),
        ResIE = ok(literal(Lit))

    ;
        E = bool_type,
        ResIE = ok(bool_type)

    ;
        E = app(M, N),
        ResIN = to_de_bruijn(Ctx, N),
        (
            ResIN = ok(IN),
            ResIM = to_de_bruijn(Ctx, M),
            (
                ResIM = ok(IM),
                ResIE = ok(app(IM, IN))
            ;
                ResIM = free_variable(_, V__, _),
                ResIE = free_variable(Ctx, V__, E)
            ;
                ResIM = hole(_, _),
                ResIE = hole(Ctx, E)
            )
        ;
            ResIN = free_variable(_, V__, _),
            ResIE = free_variable(Ctx, V__, E)
        ;
            ResIN = hole(_, _),
            ResIE = hole(Ctx, E)
        )
    ).

% I'm torn on whether to error out here or just go with it, since an error here also stops
% useful errors from being reported if they rely on this function to display them
% (and not erroring allows visually spotting the free variable in outputs)
from_de_bruijn([], var(idx(V))) = Name :- % require.unexpected($pred, "free variable: " ++ show(var(V))).
    Name = var(v("#" ++ show(V))).
% from_de_bruijn([_|_], var(zero)) = var(v(Alias)).
% if it doesn't have any aliases, we assign it one based on its type
from_de_bruijn(_, var(wildcard)) = var(wildcard).
from_de_bruijn([{yes(Alias), _}|_], var(idx(zero))) = var(v(Alias)).
from_de_bruijn([{no, _}|_], var(idx(zero))) = require.unexpected($pred, "Unnamed variable (FIXME)").
% from_de_bruijn([{no, A}|Ctx], var(zero)) = var(v(name_var(A, type_in_ctx_count(Ctx, A)))).
% from_de_bruijn([A|Ctx], var(_, zero)) = var(v(name_var(A, type_in_ctx_count(Ctx, A)))).
from_de_bruijn([_|Ctx], var(idx(succ(N)))) = from_de_bruijn(Ctx, var(idx(N))).
% FIXME: These don't discriminate between unnamed variables and wildcards
from_de_bruijn(Ctx, lambda({VarM, A}, Body)) =
    lambda(l(V, from_de_bruijn(Ctx, A)), from_de_bruijn([{VarM, A}|Ctx], Body)) :-
        ( VarM = yes(S), V = v(S) ; VarM = no, V = wildcard).
from_de_bruijn(Ctx, pi_type({VarM, A}, Body)) =
    pi_type(l(V, from_de_bruijn(Ctx, A)), from_de_bruijn([{VarM, A}|Ctx], Body)) :-
        ( VarM = yes(S), V = v(S) ; VarM = no, V = wildcard).
from_de_bruijn(_, bool_type) = bool_type.
from_de_bruijn(_, literal(X)) = literal(X).
from_de_bruijn(_, sort(S)) = sort(S).
from_de_bruijn(Ctx, app(M, N)) = app(from_de_bruijn(Ctx, M), from_de_bruijn(Ctx, N)).

% checks if the first expression is literally present anywhere in the second.
% (Does not consider alpha-equivalence, nor does it perform normalisation.)
:- pred occurs_in(expr::in, expr::in) is semidet.
occurs_in(indexed(E), indexed(E)).
occurs_in(named(E), named(E)) :- not E = var(wildcard).
occurs_in(named(E1), named(E2)) :-
    not E1 = var(wildcard),
    (
        E2 = app(M, N),
        ( occurs_in(named(E1), named(M))
        ; occurs_in(named(E1), named(N))
        )
    ;
        E2 = lambda(l(V, A), Body),
        ( if E1 = var(v(S)), V = v(S), occurs_in(named(E1), named(Body))
        then
            dbg(show(E2) ++ " shadows " ++ show(E1)),
            occurs_in(named(E1), named(A))
        else if not occurs_in(named(E1), named(A))
        then occurs_in(named(E1), named(Body))
        else false
        )
    ;
        E2 = pi_type(l(V, A), B),
        ( if E1 = var(v(S)), V = v(S), occurs_in(named(E1), named(B))
        then
            dbg(show(E2) ++ " shadows " ++ show(E1)),
            occurs_in(named(E1), named(A))
        else if not occurs_in(named(E1), named(A))
        then occurs_in(named(E1), named(B))
        else false
        )
    ).
occurs_in(indexed(E1), indexed(E2)) :-
    (
        E2 = app(M, N),
        ( occurs_in(indexed(E1), indexed(M))
        ; occurs_in(indexed(E1), indexed(N))
        )
    ;
        E2 = lambda({_, A}, Body),
        (
            occurs_in(indexed(E1), indexed(A))
        ;
            E1_ = (if E1 = var(idx(N)) then var(idx(succ(N))) else E1),
            occurs_in(indexed(E1_), indexed(Body))
        )
    ;
        E2 = pi_type({_, A}, B),
        (
            occurs_in(indexed(E1), indexed(A))
        ;
            E1_ = (if E1 = var(idx(N)) then var(idx(succ(N))) else E1),
            occurs_in(indexed(E1_), indexed(B))
        )
    ).

% get_index(Ctx, V, I) returns the number of abstractions (PI-types' bindings included) between
% the current type environment of the variable V and where it was bound starting at I
:- func get_index(type_env(named_expr), string, nat) = nat is semidet.
get_index([{V_, _}|Ctx], V, I) =
    ( if V_ = yes(V)
    then I
    else get_index(Ctx, V, succ(I))
    ).

% Gets the assigned name of a variable if any
:- func get_name(type_env(indexed_expr), index) = string is semidet.
get_name([{yes(V), _}, _], idx(zero)) = V.
get_name([_|Ctx], idx(succ(I))) = get_name(Ctx, idx(I)).

% Gets the annotated type of a variable if any
:- func get_type(type_env(indexed_expr), index) = indexed_expr is semidet.
get_type([{_, A}, _], idx(zero)) = A.
get_type([_|Ctx], idx(succ(I))) = get_type(Ctx, idx(I)).

unname(lambda({_, A}, Body)) = lambda({maybe.no, unname(A)}, unname(Body)).
unname(pi_type({_, A}, B)) = pi_type({maybe.no, unname(A)}, unname(B)).
unname(app(M, N)) = app(unname(M), unname(N)).
unname(E@literal(_)) = E.
unname(E@sort(_)) = E.
unname(E@var(_)) = E.
unname(E@bool_type) = E.
