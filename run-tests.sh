set -e

if [[ -z $1 ]]; then
    test_files="tests/*.pr"
else
    test_files=$1
fi

for f in $test_files
do
    if [[ -f $f ]]; then
        test_file=$f
    else
        test_file=tests/$f.pr
    fi
    echo ""
    echo "Evaluating $test_file:"
    echo "--------------------------"
    cat $test_file | sed '/^[[:space:]]*$/d'
    echo "--------------------------"
    ./pyro eval < $test_file && echo "" || exit 1
    echo ""
done
