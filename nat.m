:- module nat.
:- interface.
:- import_module int, show.
:- use_module enum.


:- type nat ---> succ(nat) ; zero.
:- inst nat(I) for nat/0 ---> zero ; succ(I).
:- inst nat_succ(I) for nat/0 ---> succ(I).


:- pragma terminates(nat_compare/3).
:- pred nat_compare(comparison_result, nat, nat).
:- mode nat_compare(uo, in, in) is det.

:- pragma terminates(add/2).
:- func add(nat, nat) = nat.
:- pragma check_termination(dec/1).
:- func dec(nat) = nat is semidet.

:- instance showable(nat).
:- instance enum.enum(nat).

:- pragma check_termination(clamp/3).
:- pred clamp(nat, nat, nat).
:- mode clamp(in, in, in) is semidet.

:- pragma check_termination(min/2).
:- func min(nat, nat) = nat.
:- mode min(in, in) = out is det.

:- pragma check_termination(max/2).
:- func max(nat, nat) = nat.
:- mode max(in, in) = out is det.

:- pragma terminates(nat_int/2).
:- pred nat_int(nat, int).
:- mode nat_int(in, out) is det.
:- mode nat_int(out, in) is semidet.

:- func from_int_det(int) = nat.
:- mode from_int_det(in) = out is det.

:- implementation.
:- import_module string, enum.
:- use_module require.

:- instance showable(nat) where [
    show(zero) = "0",
    show(succ(N)) = show(1 + string.det_to_int(show(N)))
].

:- instance enum.enum(nat) where [
    to_int(zero) = 0,
    to_int(succ(N)) = 1 + to_int(N),
    from_int(I) = N :-
        ( if 0 < I
        then N = succ(from_int(I - 1))
        else I = 0, N = zero
        )
].

:- pragma check_termination(clamp/3).
clamp(N, X, M) :-
    nat_compare((<), N, M),
    X = min(X, M),
    X = max(X, N).

:- pragma check_termination(min/2).
min(zero, zero) = zero.
min(zero, succ(_)) = zero.
min(succ(_), zero) = zero.
min(succ(N), succ(M)) = succ(min(N, M)).

:- pragma check_termination(max/2).
max(zero, zero) = zero.
max(zero, succ(N)) = succ(N).
max(succ(N), zero) = succ(N).
max(succ(N), succ(M)) = succ(max(N, M)).

nat_compare((=), zero, zero).
nat_compare((<), zero, succ(_)).
nat_compare((>), succ(_), zero).
nat_compare(Res, succ(N), succ(M)) :- nat_compare(Res, N, M).

add(I, zero) = I.
add(I, succ(J)) = add(succ(I), J).

:- pragma check_termination(dec/1).
dec(succ(zero)) = zero.
dec(succ(succ(N))) = N.

:- pragma promise_pure(nat_int/2).
nat_int(N::in, I::out) :- I = to_int(N).
nat_int(N::out, I::in) :- N = from_int(I).

from_int_det(I) = (if from_int(I) = N then N else require.unexpected($pred, "int was negative")).
