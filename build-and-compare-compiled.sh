set -e

mmc --make --enable-term --halt-at-warn pyro

if [[ -z $1 ]]; then
    test_files="tests/*.pr"
else
    test_files=$1
fi

for f in $test_files
do
    if [[ -f $f ]]; then
        test_file=$f
    else
        test_file=tests/$f.pr
    fi
    ./pyro compile $test_file
    hvm_file=${test_file%.pr}.hvm
    echo ""
    if [[ $(which hvm) ]]; then
        ./run-tests.sh $f
        echo "Evaluating $hvm_file:"
        echo "--------------------------"
        cat $hvm_file
        echo "--------------------------"
        hvm r $hvm_file
    else
        echo "No hvm in your path; can not compare evaluation output"
    fi
done
