:- module utils.
:- interface.
:- import_module show, string.

:- pred dbg(T::in) is det <= showable(T).
:- pred dbg(T::di, T::uo) is det <= showable(T).
:- func dbg(T) = T <= showable(T).
:- mode dbg(in) = out is det.
:- mode dbg(out) = in is det.
:- mode dbg(di) = uo is det.
:- mode dbg(uo) = di is det.
:- func dbg(string, T) = T <= showable(T).
:- mode dbg(di, in) = out is det.
:- mode dbg(di, out) = in is det.
:- mode dbg(di, di) = uo is det.
:- mode dbg(di, uo) = di is det.

:- pred dbgw(T::in) is det.
:- pred dbgw(T::di, T::uo) is det.
:- func dbgw(T) = T.
:- mode dbgw(in) = out is det.
:- mode dbgw(out) = in is det.
:- mode dbgw(di) = uo is det.
:- mode dbgw(uo) = di is det.
:- func dbgw(string, T) = T <= showable(T).
:- mode dbgw(di, in) = out is det.
:- mode dbgw(di, out) = in is det.
:- mode dbgw(di, di) = uo is det.
:- mode dbgw(di, uo) = di is det.

:- func copy(T) = T.
:- mode copy(ui) = uo is det.
:- mode copy(di) = out is det.
:- mode copy(in) = uo is det.

:- pred dup(T::uo, T::di, T::uo) is det.

:- implementation.
:- use_module io.

:- pragma promise_pure(dbg/1).
dbg(X) :-
    semipure io.unsafe_get_io_state(IO0),
    io.print_line(show(X), IO0, _).

:- pragma promise_pure(dbg/2).
dbg(X0, X_) :-
    semipure io.unsafe_get_io_state(IO0),
    copy(X0, X_),
    io.print_line(show(X0), IO0, _).

dbg(X0::di) = (X_::uo) :-
    semipure io.unsafe_get_io_state(IO0),
    copy(X0, X_),
    io.print_line(show(X0), IO0, _).
dbg(X::in) = (X::out) :-
    semipure io.unsafe_get_io_state(IO0),
    io.print_line(show(X), IO0, _).
dbg(X0::uo) = (X_::di) :-
    semipure io.unsafe_get_io_state(IO0),
    copy(X_, X0),
    io.print_line(show(X_), IO0, _).
dbg(X::out) = (X::in) :-
    semipure io.unsafe_get_io_state(IO0),
    io.print_line(show(X), IO0, _).

dbg(Msg::di, X0::di) = (X_::uo) :-
    semipure io.unsafe_get_io_state(IO0),
    copy(X0, X_),
    io.print_line(Msg ++ " " ++ show(X0), IO0, _).
dbg(Msg::di, X::in) = (X::out) :-
    semipure io.unsafe_get_io_state(IO0),
    io.print_line(Msg ++ " " ++ show(X), IO0, _).
dbg(Msg::di, X0::uo) = (X_::di) :-
    semipure io.unsafe_get_io_state(IO0),
    copy(X_, X0),
    io.print_line(Msg ++ " " ++ show(X_), IO0, _).
dbg(Msg::di, X::out) = (X::in) :-
    semipure io.unsafe_get_io_state(IO0),
    io.print_line(Msg ++ " " ++ show(X), IO0, _).

:- pragma promise_pure(dbgw/1).
dbgw(X) :-
    semipure io.unsafe_get_io_state(IO0),
    io.write_line(X, IO0, _).

:- pragma promise_pure(dbgw/2).
dbgw(X0, X_) :-
    semipure io.unsafe_get_io_state(IO0),
    copy(X0, X_),
    io.write_line(X0, IO0, _).

dbgw(X0::di) = (X_::uo) :-
    semipure io.unsafe_get_io_state(IO0),
    copy(X0, X_),
    io.write_line(X0, IO0, _).
dbgw(X::in) = (X::out) :-
    semipure io.unsafe_get_io_state(IO0),
    io.write_line(X, IO0, _).
dbgw(X0::uo) = (X_::di) :-
    semipure io.unsafe_get_io_state(IO0),
    copy(X_, X0),
    io.write_line(X_, IO0, _).
dbgw(X::out) = (X::in) :-
    semipure io.unsafe_get_io_state(IO0),
    io.write_line(X, IO0, _).

dbgw(Msg::di, X0::di) = (X_::uo) :-
    semipure io.unsafe_get_io_state(IO0),
    copy(X0, X_),
    io.print(Msg ++ " ", IO0, IO1),
    io.write_line(X0, IO1, _).
dbgw(Msg::di, X::in) = (X::out) :-
    semipure io.unsafe_get_io_state(IO0),
    io.print(Msg ++ " ", IO0, IO1),
    io.write_line(X, IO1, _).
dbgw(Msg::di, X0::uo) = (X_::di) :-
    semipure io.unsafe_get_io_state(IO0),
    copy(X_, X0),
    io.print(Msg ++ " ", IO0, IO1),
    io.write_line(X_, IO1, _).
dbgw(Msg::di, X::out) = (X::in) :-
    semipure io.unsafe_get_io_state(IO0),
    io.print(Msg ++ " ", IO0, IO1),
    io.write_line(X, IO1, _).

copy(X0) = X1 :- copy(X0, X1).
dup(X1, X0, X2) :-
    copy(X0, X1),
    copy(X0, X2).
