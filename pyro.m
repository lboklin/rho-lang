:- module pyro.
:- interface.
:- use_module io.

:- pred main(io.io::di, io.io::uo) is cc_multi.

:- implementation.
:- import_module expr, show, utils, string, list.
:- use_module compile, check_reduce, pprint, read, parsing_utils, require, exception.


main(!IO) :-
    io.command_line_arguments(Args, !IO),
    (
        Args = [],
        io.read_file_as_string(io.stdin_stream, Res, !IO),
        io.close_input(io.stdin_stream, !IO),
        (
            Res = io.ok(Str),
            io.print(Str ++ "#(=> ", !IO), read_source(Str, !IO), io.print_line(")", !IO)
        ;
            Res = io.error(Msg, Err),
            io.format("Couldn't read stdin: %s, %s", [s(Msg), s(io.error_message(Err))], !IO),
            io.set_exit_status(1, !IO)
        )
    ;
        Args = [Arg | Rest],
        (
            if Arg = "repl"
            then repl(!IO)
            else if Arg = "compile"
            then
                ( if Rest = [Filename]
                then compile.compile(Filename, !IO)
                else
                    io.read_line_as_string(io.stdin_stream, ResFilename, !IO),
                    ( if ResFilename = io.ok(Filename)
                    then compile.compile(Filename, !IO)
                    else
                        io.print_line("pyro compile takes one argument.", !IO),
                        io.set_exit_status(1, !IO)
                    )
                )
            else if Arg = "eval"
            then
                ( if Rest = [Str]
                then read_source(Str, !IO)
                else if Rest = []
                then
                    (
                        io.read_file_as_string(io.stdin_stream, Res, !IO),
                        io.close_input(io.stdin_stream, !IO),
                        (
                            Res = io.ok(Str),
                            read.read(Str, Result),
                            ( if Result = read.ok(E, _)
                            then io.print(show(named(from_de_bruijn([], E))), !IO)
                            else io.print_line("eval failed: " ++ show(Result), !IO), io.set_exit_status(1, !IO)
                            )
                        ;
                            Res = io.error(Msg, Err),
                            io.format("Couldn't read stdin: %s\n%s", [s(Msg), s(io.error_message(Err))], !IO),
                            io.set_exit_status(1, !IO)
                        )
                    )
                else io.print_line("eval expects one expression", !IO), io.set_exit_status(1, !IO)
                )
            else
                parse_file(Arg, !IO)
        )
    ).

:- pred parse_file(string::in, io.io::di, io.io::uo) is cc_multi.
parse_file(FileName, !IO) :-
    io.open_input(FileName, InputRes, !IO),
    ( InputRes = io.ok(InputStream),
      io.read_file_as_string(InputStream, Res, !IO),
      io.close_input(io.stdin_stream, !IO),
      ( Res = io.ok(String),
        read_source(String, !IO)
      ; Res = io.error(Msg, Err),
        io.format("Couldn't read file: %s (%s, %s)",
            [s(FileName), s(Msg), s(io.error_message(Err))],
            !IO)
      )
    ; InputRes = io.error(_),
      io.print_line("Couldn't open file: " ++ FileName, !IO)
    ).

:- pred repl(io.io::di, io.io::uo) is cc_multi.
repl(!IO) :-
    io.write_string("[𝞺] ", !IO),
    io.flush_output(!IO),
    io.read_line_as_string(Res, !IO),
    (
        Res = io.error(_),
        io.write_string("Error reading from stdin\n", !IO)
    ;
        Res = io.eof,
        io.write_string("EOF\n", !IO)
    ;
        Res = io.ok(Line),
        read.read(Line, Result),
        ( if Result = read.ok(E, A)
        then
            ClrdStr =
                show_coloured(named(from_de_bruijn([], E))) ++
                "\n  : " ++ show_coloured(named(from_de_bruijn([], A))),
            pprint.print_coloured(ClrdStr, !IO)
        else io.print_line(show(Result), !IO)
        ),
        repl(!IO) % recursively call ourself for the next line(s)
    ).

:- pred read_source(string::in, io.io::di, io.io::uo) is cc_multi.
read_source(String, !IO) :-
    read.read(String, Result),
    ( if Result = read.ok(E_, A)
    then
        R_ = normalise(E_),
        read.read_no_checkred(String, Result_),
        ( if Result_ = parsing_utils.ok(E)
        then pretty_print(E, R_, A, !IO)
        else require.unexpected($pred, "Parsing failed after already passing once for: " ++ String)
        )
    else require.unexpected($pred, "Failed to parse expression.\n" ++ show(Result))
    ).

:- pred pretty_print(named_expr::in, indexed_expr::in, indexed_expr::in, io.io::di, io.io::uo) is det.
pretty_print(E, E_, A, !IO) :-
    io.print_line("===========", !IO),
    % io.print_line(show(E), !IO),
    pprint.print_coloured(show_coloured(named(E)), !IO),
    ResIE = to_de_bruijn([], E),
    ( if ResIE = ok(IE)
    then pprint.print_coloured(show_coloured(indexed(IE)), !IO)
    else io.print("wtf", !IO)
    ),
    io.print_line("===========", !IO),
    io.print_line("Check-Reduced  ", !IO),
    io.print_line("-----------", !IO),
    io.print_line("De Bruijn:", !IO),
    pprint.print_coloured(show_coloured(indexed(E_)), !IO),
    pprint.print_coloured("  : " ++ show_coloured(indexed(A)), !IO),
    io.print_line("           ", !IO),
    io.print_line("Renamed:", !IO),
    pprint.print_coloured(show_coloured(named(from_de_bruijn([], E_))), !IO),
    pprint.print_coloured("  : " ++ show_coloured(named(from_de_bruijn([], A))), !IO),
    ( if check_reduce.type_of([], A) = check_reduce.ok(S)
    then pprint.print_coloured("  : " ++ show_coloured(named(from_de_bruijn([], S))), !IO)
    else io.print_line("  : ?", !IO)
    ).
