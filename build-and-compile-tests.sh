set -e

mmc --make --enable-term --halt-at-warn pyro

if [[ -z $1 ]]; then
    test_files="tests/*.pr"
else
    test_files=$1
fi

for f in $test_files
do
    if [[ -f $f ]]; then
      test_file=$f
    else
      test_file=tests/$f.pr
    fi
    echo ""
    echo "=== Compiling $test_file ==="
    cat $test_file
    ./pyro compile $test_file
    hvm_file=${test_file%.pr}.hvm
    echo "=== Compilation output ==="
    cat $hvm_file
    echo "--------------------------"
    if [[ $(which hvm) ]]; then
        hvm r $hvm_file
    fi
done
