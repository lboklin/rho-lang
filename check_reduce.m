% An injective Pure Type System using Barthe's algorithm for a "sound and complete syntax directed system".
% Based on "Pure Type Systems for Functional Programming" by Jan-Willem Roorda & Johan Jeuring, 2000.
:- module check_reduce.
:- interface.
:- import_module expr, show, string.

% Resulting type or the ill-typed expression with an explanation
:- type type_result
  ---> ok(indexed_expr)
     ; type_error(type_env(indexed_expr), indexed_expr, string).
:- instance showable(type_result).

:- type result
  ---> ok(indexed_expr, indexed_expr)
     ; type_error(type_env(indexed_expr), indexed_expr, string).
:- instance showable(result).

:- func type_of(type_env(indexed_expr), indexed_expr) = type_result is det.
:- func check_reduce(type_env(indexed_expr), indexed_expr) = result is det.


:- implementation.
:- import_module nat, utils, list.
:- use_module maybe, require.

% The axiom rule as a functional PTS, for any given S1, there is at most one S2 such that axiom(S1, S2) holds.
% For an injective PTS, for any S2, there is at most one S1 such that axiom(S1, S2) holds.
% ( axiom(S1, S2), axiom(S1_, S2) ) => S1 = S1_,
:- pred axiom(sort, sort).
:- mode axiom(in, in) is semidet.
:- mode axiom(in, out) is det. % det since we don't have a limit for the number of universes
:- mode axiom(out, in) is semidet.

:- instance showable(type_result) where [
    show(ok(A)) = show(A),
    show(type_error(Ctx, E, Desc)) = show(type_error(Ctx, E, Desc) : result)
].

:- instance showable(result) where [
    show(ok(E, A)) = show(from_de_bruijn([], E)) ++ " : " ++ show(from_de_bruijn([], A)),
    show(type_error(Ctx, E, Desc)) =
        "Type error in expression:\n" ++ show(from_de_bruijn(Ctx, E)) ++
        "\nType environment:\n" ++ show_context(Ctx) ++
        "\nDescription:\n" ++ Desc
].

check_reduce(Ctx, E) = Res :-
    ResType = type_of(Ctx, E),
    (
        ResType = ok(A),
        Res = ok(normalise(E), A)
    ;
        ResType = type_error(Ctx_, M, Desc),
        Res = type_error(Ctx_, M, Desc)
    ).

:- pred unify_types(type_env(indexed_expr), indexed_expr, indexed_expr).
:- mode unify_types(in, in, in) is semidet.
unify_types(Ctx, T1, T2) :- unify_types(Ctx, T1, T2) = _ : indexed_expr.
% Returns the normalised version of the two types if they are equal.
% NB. it removes any names assigned to variables in the process.
:- func unify_types(type_env(indexed_expr), indexed_expr, indexed_expr) = indexed_expr is semidet.
unify_types(Ctx, T1, T2) = T :-
    ( if T1 = var(wildcard)
    then check_reduce(Ctx, T2) = ok(T, _)
    else if T2 = var(wildcard)
    then check_reduce(Ctx, T1) = ok(T, _)
    else if
        T1 = pi_type({_, A1}, B1),
        T2 = pi_type({_, A2}, B2)
    then
        A = unify_types(Ctx, A1, A2),
        B = unify_types([{maybe.no, A}|Ctx], B1, B2),
        T = pi_type({maybe.no, A}, B)
    else
        check_reduce(Ctx, unname(T1)) = ok(A1, S),
        check_reduce(Ctx, unname(T2)) = ok(A2, S),
        ( if A1 = A2
        then T = A1
        else T = unify_types(Ctx, A1, A2)
        )
    ).

% The classification based rules, based on the classification algorithm.
% ---
% axiom rule
type_of(_, sort(S1)) = ok(sort(S2)) :-
    axiom(S1, S2).

% start rule: if a variable's type is in context, its sort also is.
type_of([], V@var(_)) = type_error([], V, "Free variable").
type_of([_|Ctx], var(V)) = Res :-
    (
        V = idx(succ(N)),
        ARes = type_of(Ctx, var(idx(N))),
        Res = (if ARes = ok(A) then ok(update(A, zero, succ(zero))) else ARes)
    ;
        % V = idx(zero)
        V = wildcard,
        Res = type_error([], var(wildcard), "Wilcard")
    ).
type_of(Ctx@[{_, A}|Ctx_], var(idx(zero))) = Res :-
    % A must be of a decidable sort
    ( if _ = sort_of(Ctx_, A)
    then Res = ok(normalise(update(A, zero, succ(zero))))
    else
        AStr = show(from_de_bruijn(Ctx, var(idx(zero)))),
        ErrMsg =
            "No sort could be induced from context for the type in\n" ++
            AStr ++ " : " ++ show(A),
        Res = type_error(Ctx, var(idx(zero)), ErrMsg)
    ).

% abs(traction) rule
type_of(Ctx, E@lambda(Ann@{_, A}, Body)) = Res :-
    ( if S1 = sort_of(Ctx, A)
    then
        % the sort S1 of the domain of the abstraction and the sort S2 of its codomain
        % are such that there exists an axiom rule (S1, S2, S3)
        ( if S2 = sort_of_sort_of([Ann|Ctx], Body)
        then
            pi_sort(S1, S2) = _,
            type_of([Ann|Ctx], Body) = BRes,
            (
                BRes = ok(B),
                Res = ok(normalise(pi_type(Ann, B)))
            ;
                BRes = type_error(_, _, _),
                Res = type_error(Ctx, E, show(BRes))
            )
        else if
            Body = lambda(BodyAnn@{_, var(wildcard)}, BodyBody),
            % pi_sort(S1, S2) = _,
            % pi_sort(_, BodyS2) = S2,
            %   :- dependency_rule(S1, BodyS2, S2)
            %   :- dependency_rule(S1, BodyS2), dependency_rule(S1, S2)
            pi_sort(S1, BodyS2) = _,
            BodyS2 = sort_of_sort_of([BodyAnn|[Ann|Ctx]], BodyBody),
            BodyBRes = type_of([BodyAnn|[Ann|Ctx]], BodyBody)
        then
            (
                BodyBRes = ok(BodyB),
                Res = ok(normalise(pi_type(Ann, pi_type(BodyAnn, BodyB))))
            ;
                BodyBRes = type_error(_, _, _),
                Res = type_error(Ctx, E,
                    "Could not determine the function's codomain, possibly because of an elided type:\n" ++
                    show(BodyBRes))
            )
        else
            BodyStr = show(from_de_bruijn([Ann|Ctx], Body)),
            BRes = type_of([Ann|Ctx], Body),
            (
                BRes = ok(B),
                BStr = show(from_de_bruijn([Ann|Ctx], B)),
                Res = type_error([Ann|Ctx], Body, "(" ++ BodyStr ++ ")\n\t: " ++ BStr ++ "\n\t: ?")
            ;
                BRes = type_error(_,_,_),
                Res = type_error([Ann|Ctx], Body,
                    "Could not determine the function's codomain for the following reason:\n" ++
                    show(BRes))
            )
        )
    else if
        A = var(wildcard),
        _ = sort_of_sort_of([Ann|Ctx], Body),
        BRes = type_of([Ann|Ctx], Body)
    then
        (
            BRes = ok(B),
            Res = ok(normalise(pi_type(Ann, B)))
        ;
            BRes = type_error(_, _, _),
            Res = type_error(Ctx, E,
                "Could not determine the function's elided domain:\n" ++
                show(BRes))
        )
    else
        AStr = show(from_de_bruijn(Ctx, A)),
        Res = type_error(Ctx, E, "The function's domain:\n" ++ AStr ++ "\nis not of a typeable sort.")
    ).

% pi rule
type_of(Ctx, pi_type(Ann@{_, A}, Body)) = Res :-
    ( if sort_of(Ctx, A) = S1
    then
        ( if sort_of([Ann|Ctx], Body) = S2
        then
            dependency_rule(S1, S2, S3),
            Res = ok(sort(S3))
        else
            Res =
                type_error(Ctx, pi_type(Ann, Body),
                    "There is no valid sort for this dependent function type:\n" ++
                    show(from_de_bruijn(Ctx, pi_type(Ann, Body))) ++
                    "\nIt's likely the expression returns a value on the term-level rather than the type- or a higher sorted-level,\n" ++
                    "or possibly a sort higher than allowed"
                )
        )
    else
        Res =
            type_error(Ctx, A,
                "The sort of this dependent function type's codomain:\n" ++
                show(from_de_bruijn(Ctx, A)) ++
                "\nis not valid."
            )
    ).

% app(lication) rule
type_of(Ctx, app(M_, N_)) = Res :-
    {M, N, T} = plug_hole(Ctx, M_, N_),
    E = app(M, N),
    ( if T = maybe.yes(ResN)
    then NRes = ResN
    else NRes = type_of(Ctx, N)
    ),
    (
        NRes = ok(NType_),
        MRes = type_of(Ctx, M),
        (
            MRes = ok(MType_),
            ( if
                MType_ = pi_type({_, A_}, B_),
                unify_types(Ctx, NType_, A_)
            then Res = ok(normalise(substitute(B_, zero, N)))
            % it gets stuck here while trying to make an error for some reason?!
            else if MType_ = pi_type({_, A_}, _)
            then
                Res =
                    type_error(Ctx, E,
                      "Mismatch between function domain\n" ++
                      show(from_de_bruijn(Ctx, A_)) ++
                      "\nand argument type\n" ++
                      show(from_de_bruijn(Ctx, NType_))
                  )
            else Res = type_error(Ctx, E, "Something went wrong with " ++ show(MType_))
            )
        ;
            MRes = type_error(_,_,_),
            Res = type_error(Ctx, E, "Could not determine the function's type for the following reason:\n" ++ show(MRes))
        )
    ;
        NRes = type_error(_, _, _),
        Res = type_error(Ctx, E, "Could not determine the argument's type for the following reason:\n" ++ show(NRes))
    ).

% our extension: builtins
type_of(_, bool_type) = ok(sort(typ(zero))).
type_of(_, literal(boolean(_))) = ok(bool_type).
% ---

% % Applying a lambda with a type hole directly to an argument allows us to infer the type
:- func plug_hole(type_env(indexed_expr), indexed_expr, indexed_expr) =
    {
        indexed_expr,
        indexed_expr,
        maybe.maybe(check_reduce.type_result)
    } is det.
plug_hole(Ctx, M_, N) = {M, N, T} :-
    ( if M_ = lambda({V, var(wildcard : index)}, Body)
    then
        ResA = type_of(Ctx, N),
        ( if ResA = ok(A)
        then M = lambda({V, A}, Body)
        else M = M_
        ),
        T = maybe.yes(ResA)
    else
        M = M_,
        T = maybe.no
    ).

% ---

axiom(typ(N), typ(succ(N))).

% The dependency rules for the sorts of dependent function types.
% ---
% dependency_rule(S1, S2, S3)
% S1 is the sort of the domain
% S2 is the sort of the codomain
% S3 is the sort of the pi-type itself
% NB:
% A PTS is functional if given any (S1, S2) we have at most one S3 such that
%   dependency_rule(S1, S2, S3)
% holds, i.e.
%   ( dependency_rule(S1, S2, S3), dependency_rule(S1, S2, S3_) ) => S3 = S3_
%
% A PTS is injective if its functional and if given any (S1, S3) we have at most one S2 such that
%   dependency_rule(S1, S2, S3)
% holds, i.e.
%   ( dependency_rule(S1, S2, S3), dependency_rule(S1, S2_, S3) ) => S2 = S2_
% ---
% First the dependency rules for the sorts of non-dependent function types.
% For example:
% (0, 0) is terms to terms,
% (1, 0) is types to terms,
% (0, 2) is terms to kinds.
:- pred dependency_rule(sort, sort).
:- mode dependency_rule(in, in) is det.
% I say anything is possible!
dependency_rule(_, _).
% and thus, these are det rather than semidet
:- pred dependency_rule(sort, sort, sort).
:- mode dependency_rule(in, out, in) is det.
:- mode dependency_rule(in, in, out) is det.
:- mode dependency_rule(in, in, in) is semidet.
% This makes for a simple injective PTS
dependency_rule(S1, S2, S2) :- dependency_rule(S1, S2).
% alternatively one could do something like this (except this doesn't work):
% dependency_rule(S1@typ(L1), S2@typ(min(L1, L3)), typ(max(L1, L2))) :- dependency_rule(S1, S2).

% Mappings from sorts to sorts.
% ---
:- func sort_plus(sort) = sort is det.
sort_plus(S1) = S2 :- axiom(S1, S2).
:- func sort_minus(sort) = sort is semidet.
sort_minus(S2) = S1 :- axiom(S1, S2).
% called ρ in the paper
:- func pi_sort(sort, sort) = sort.
:- mode pi_sort(in, in) = out is det.
:- mode pi_sort(in, out) = in is det.
:- mode pi_sort(in, in) = in is semidet.
pi_sort(S1, S2) = S3 :- dependency_rule(S1, S2, S3).
% called μ in the paper
:- func dependent_sort(sort, sort) = sort is det.
dependent_sort(S1, S2) = S3 :- dependency_rule(S1, S3, S2).
% ---

% Classification algorithm.
% ---
% sort_of(Ctx, E) = S
% S is the sort of the expression E as induced from context Ctx.
%
% sort_of_sort_of(Ctx, E) = S
% S is the sort of the sort of the expression E as induced from context Ctx.
%
% They correspond to the 'classification lemma' from the paper:
% Γ ⊢ M : s & Γ ⊢ s : S ⇒ sort(Γ|M) = s
% Γ ⊢ M : A & Γ ⊢ A : s ⇒ elmt(Γ|M) = s

:- func sort_of(type_env(indexed_expr), indexed_expr) = sort is semidet.
sort_of(Ctx, V@var(_)) = sort_minus(sort_of_sort_of(Ctx, V)).
sort_of(_, sort(S)) = sort_plus(S).
sort_of(Ctx, MN@app(_, _)) = sort_minus(sort_of_sort_of(Ctx, MN)).
sort_of(Ctx, F@lambda(_, _)) = sort_minus(sort_of_sort_of(Ctx, F)).
sort_of(Ctx, pi_type(Ann@{_, A}, B)) = pi_sort(sort_of(Ctx, A), sort_of([Ann|Ctx], B)).
sort_of(_, bool_type) = typ(zero).

:- func sort_of_sort_of(type_env(indexed_expr), indexed_expr) = sort is semidet.
sort_of_sort_of([{_, A}|Ctx], var(idx(zero))) = sort_of(Ctx, A).
sort_of_sort_of([_|Ctx], var(idx(succ(N)))) = sort_of_sort_of(Ctx, var(idx(N))).
sort_of_sort_of(_, sort(S)) = sort_plus(sort_plus(S)).
sort_of_sort_of(Ctx, app(M_, N_)) = dependent_sort(sort_of_sort_of(Ctx, N), sort_of_sort_of(Ctx, M)) :-
    % Can we use a returned type here without changing the behaviour?
    % (e.g. sort_of(Ctx, A) instead of sort_of_sort_of(Ctx, N))
    {M, N, _} = plug_hole(Ctx, M_, N_).
sort_of_sort_of(Ctx, lambda(Ann@{_, A}, M)) = pi_sort(sort_of(Ctx, A), sort_of_sort_of([Ann|Ctx], M)).
sort_of_sort_of(Ctx, P@pi_type(_, _)) = sort_plus(sort_of(Ctx, P)).
sort_of_sort_of(_, bool_type) = typ(succ(zero)).
sort_of_sort_of(_, literal(_)) = typ(zero).
% ---


%%% UTILS

:- func dbgi(type_env(indexed_expr), indexed_expr) = indexed_expr.
dbgi(Ctx, E) = E :- dbg(from_de_bruijn(Ctx, E)).
:- func dbgi(string, type_env(indexed_expr), indexed_expr) = indexed_expr.
:- mode dbgi(di, in, in) = out.
dbgi(Msg, Ctx, E) = E :- _ = dbg(Msg, from_de_bruijn(Ctx, E)).
