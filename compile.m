:- module compile. % to HVM
:- interface.
:- import_module expr, string.
:- use_module io.

:- pred compile_to(string::in, named_expr::in, io.io::di, io.io::uo) is det.
:- pred compile(string::in, io.io::di, io.io::uo) is cc_multi.

:- implementation.
:- import_module show, nat, list, bool, check_reduce.
:- use_module enum, read, parsing_utils.

:- type hvm_ast
  ---> var(string)
     % dup(string, string, hvm_ast, hvm_ast)
     ; let(string, hvm_ast, hvm_ast)
     ; lam(string, hvm_ast)
     ; app(hvm_ast, hvm_ast)
     ; ctr(string, list(hvm_ast))
     ; u32(int)
     % op2(oper, hvm_ast, hvm_ast)
     .

:- func to_hvm_id(var) = string.
to_hvm_id(v(V)) = "_" ++ V_ :-
    string.replace_all(V, "-", "_", V0),
    string.replace_all(V0, "'", "_prime_", V_).
to_hvm_id(wildcard) = "_".

:- func to_hvm(named_expr) = hvm_ast.
to_hvm(var(V)) = var(to_hvm_id(V)).
% to_hvm(lambda(l(wildcard, _), E)) = lam("_", to_hvm(E)).
to_hvm(lambda(l(V, _), E)) = lam(to_hvm_id(V), to_hvm(E)).
to_hvm(pi_type(l(wildcard, A), E)) = ctr("PiT", [to_hvm(A), to_hvm(E)]).
to_hvm(pi_type(l(V@v(_), A), E)) =
    ctr("PiT", [to_hvm(A), lam(to_hvm_id(V), to_hvm(E))]).
to_hvm(app(E1, E2)) = app(to_hvm(E1), to_hvm(E2)).
to_hvm(sort(typ(N))) = ctr("Type", [u32(enum.to_int(N))]).
to_hvm(bool_type) = ctr("Bool", []).
to_hvm(literal(boolean(yes))) = u32(1).
to_hvm(literal(boolean(no))) = u32(0).

:- instance showable(hvm_ast) where [
    show(var(S)) = S,
    show(let(S, E1, Body)) = string.format("let %s = %s; %s", [s(S), s(show(E1)), s(show(Body))]),
    show(lam(V, E)) = "λ" ++ V ++ "(" ++ show(E) ++ ")",
    show(app(E1, E2)) = "(" ++ show(E1) ++ " " ++ show(E2) ++ ")",
    show(ctr(S, Es)) = Str :-
        (
            Es = [E|Rest],
            Str =
                string.format("(%s %s)",
                    [s(S), s(list.foldl(func(E_, Acc) = Acc ++ " " ++ show(E_), Rest, show(E)))])
        ;
            Es = [],
            Str = S
        ),
    show(u32(I)) = show(I)
].

compile_to(FilenameOut, E, !IO) :-
    HVME = to_hvm(E),
    io.open_output(FilenameOut, ResOutStream, !IO),
    (
        ResOutStream = io.ok(OutStream),
        OutString = "Main = " ++ show(HVME),
        io.write_string(OutStream, OutString, !IO),
        io.nl(OutStream, !IO),
        io.close_output(OutStream, !IO)
    ;
        ResOutStream = io.error(Err),
        io.print_line(show(Err), !IO),
        io.set_exit_status(1, !IO)
    ).

compile(Filename_, !IO) :-
    Filename = string.strip(Filename_), % remove surrounding whitespace for sanity
    io.open_input(Filename, ResTextStream, !IO),
    (
        ResTextStream = io.ok(InputStream),
        io.read_file_as_string(InputStream, ResSource, !IO),
        ( if ResSource = io.ok(Str)
        then
            read.read_no_checkred(Str, ParseRes),
            ( if ParseRes = parsing_utils.ok(E)
            then
                ResIE = to_de_bruijn([], E),
                ( if ResIE = ok(IE)
                then
                    TypeRes = type_of([], IE),
                    ( if TypeRes = ok(_)
                    then
                        FilenameOut = string.remove_suffix_if_present(".pr", Filename) ++ ".hvm",
                        compile_to(FilenameOut, E, !IO)
                    else
                        io.print_line("Typecheck failed.\n" ++ show(ParseRes), !IO),
                        io.set_exit_status(1, !IO)
                    )
                else
                    io.print(show(ResIE), !IO),
                    io.set_exit_status(1, !IO)
                )
            else
                io.print_line("Parsing failed.\n" ++ show(ParseRes), !IO),
                io.set_exit_status(1, !IO)
            )
        else
            io.print_line("Reading from file failed.\n" ++ show(ResSource), !IO),
            io.set_exit_status(1, !IO)
        )
    ;
        ResTextStream = io.error(Err),
        io.print_line("Couldn't open " ++ Filename ++ ".\n" ++ show(Err), !IO),
        io.set_exit_status(1, !IO)
     ).
