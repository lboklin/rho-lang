set -e

mmc --make --enable-term --halt-at-warn pyro 1>/dev/null
./run-tests.sh $@
